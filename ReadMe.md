# tc-submission-api-wrapper

Wrapper library for topcoder submissions API

# Usage

1. Include the wrapper in package.json file (of the project where you want to use the wrapper):
   `tc-submission-api-wrapper: "<github-repo>"`
2. Create an instance of this wrapper

```js
const submissionAPI = require("tc-submission-api-wrapper");
const submissionAPIClient = submissionAPI(config);
```

### Configuration variables

config object used in the above example, should have these properties:

    * AUTH0_URL - Auth0 URL. Required.
    * AUTH0_AUDIENCE - Auth0 Audience. Required.
    * TOKEN_CACHE_TIME - Time to cache the token. Optional.
    * AUTH0_CLIENT_ID - Auth0 Client id. Required.
    * AUTH0_CLIENT_SECRET - Auth0 Client secret. Required.
    * SUBMISSION_API_URL - Submission API URL. Required. \* AUTH0_PROXY_SERVER_URL - Auth0 Proxy Server URL. Optional.

E.g.
<a id="getting-started"></a>

```js
const config = {
  AUTH0_URL: "https://topcoder-dev.auth0.com/oauth/token",
  AUTH0_AUDIENCE: "https://m2m.topcoder-dev.com/",
  AUTH0_CLIENT_ID: "###masked###",
  AUTH0_CLIENT_SECRET: "###masked###",
  SUBMISSION_API_URL: "https://api.topcoder-dev.com/v5"
};

const submissionAPI = require("tc-submission-api-wrapper");
const submissionAPIClient = submissionAPI(config);

submissionAPIClient
  .getReviewTypes()
  .then(response => {
    // Use the response here
  })
  .catch(error => {
    // Handle the error here
  });
```

## Running Unit Tests

For running the tests, you can prefix all the config variables with `TEST_` and set it in the environmental variables

1. Set environmental variables

```bat
// Windows
set TEST_AUTH0_URL=https://topcoder-dev.auth0.com/oauth/token
set TEST_AUTH0_AUDIENCE=https://m2m.topcoder-dev.com/
set TEST_AUTH0_CLIENT_ID=###masked###
set TEST_AUTH0_CLIENT_SECRET=###masked###
set TEST_SUBMISSION_API_URL=https://api.topcoder-dev.com/v5
```

```bash
// Linux/Mac
export TEST_AUTH0_URL=https://topcoder-dev.auth0.com/oauth/token
export TEST_AUTH0_AUDIENCE=https://m2m.topcoder-dev.com/
export TEST_AUTH0_CLIENT_ID=###masked###
export TEST_AUTH0_CLIENT_SECRET=###masked###
export TEST_SUBMISSION_API_URL=https://api.topcoder-dev.com/v5
```

2. Run test cases. `npm test` or `npm run test:debug` for debug mode

## Wrapper methods

These methods are available in the submissionAPIClient object once you create it by following this [example](#getting-started).

| Method                                                              | Http request                      | Description                                                                             |
| ------------------------------------------------------------------- | --------------------------------- | --------------------------------------------------------------------------------------- |
| [getReviewTypes](#method-explaination-getReviewTypes)               | GET reviewTypes/                  | Get reviewTypes from submission API                                                     |
| [headReviewTypes](#method-explaination-headReviewTypes)             | HEAD reviewTypes/                 | Get only response status and headers information but no response body for the endpoint. |
| [postReviewTypes](#method-explaination-postReviewTypes)             | POST reviewTypes/                 | Post new reviewType to the submission API                                               |
| [getReviewTypesById](#method-explaination-getReviewTypesById)       | GET reviewTypes/{reviewTypeId}    | Get existing reviewType by id                                                           |
| [headReviewTypesById](#method-explaination-headReviewTypesById)     | HEAD reviewTypes/{reviewTypeId}   | Head existing reviewType by id                                                          |
| [putReviewTypesById](#method-explaination-putReviewTypesById)       | PUT reviewTypes/{reviewTypeId}    | Put existing reviewType by id                                                           |
| [patchReviewTypesById](#method-explaination-patchReviewTypesById)   | PATCH reviewTypes/{reviewTypeId}  | Patch existing reviewType by id                                                         |
| [deleteReviewTypesById](#method-explaination-deleteReviewTypesById) | DELETE reviewTypes/{reviewTypeId} | Delete existing reviewType by id                                                        |

<a id="method-explaination-getReviewTypes"></a>

### getReviewTypes

> Get all review types.

Usage:

```js
const submissionAPI = require("tc-submission-api-wrapper");
const submissionAPIClient = submissionAPI(config);

submissionAPIClient
  .getReviewTypes()
  .then(response => {
    // Use the response here
  })
  .catch(error => {
    // Handle the error here
  });
```

Params:

| Name  | Type   | Description                                                                                                                                                                                                                                                        |
| ----- | ------ | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| param | Object | Optional query params. Format should be { `page`: integer - The page number, `perPage`: integer - The number of items to list per page, `name`: string - The name filter for review types, `isActive`: boolean - The active boolean flag filter for review types } |

<a id="method-explaination-headReviewTypes"></a>

### headReviewTypes

> Get only response status and headers information but no response body for the endpoint.

Usage:

```js
const submissionAPI = require("tc-submission-api-wrapper");
const submissionAPIClient = submissionAPI(config);

submissionAPIClient
  .headReviewTypes()
  .then(response => {
    // Use the response here
  })
  .catch(error => {
    // Handle the error here
  });
```

Params:

| Name  | Type   | Description                                                                                                                                                                                                                                                        |
| ----- | ------ | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| param | Object | Optional query params. Format should be { `page`: integer - The page number, `perPage`: integer - The number of items to list per page, `name`: string - The name filter for review types, `isActive`: boolean - The active boolean flag filter for review types } |

<a id="method-explaination-postReviewTypes"></a>

### postReviewTypes

> Create a review type

Usage:

```js
const submissionAPI = require("tc-submission-api-wrapper");
const submissionAPIClient = submissionAPI(config);

const reqBody = {
  name: "Iterative Review",
  isActive: true
};
submissionAPIClient
  .postReviewTypes(reqBody)
  .then(response => {
    // Use the response here
  })
  .catch(error => {
    // Handle the error here
  });
```

Params:

| Name    | Type   | Description                                                                                     |
| ------- | ------ | ----------------------------------------------------------------------------------------------- |
| reqBody | Object | Required. The requestType that is to be created. Refer the above example for the object format. |

<a id="method-explaination-getReviewTypesById"></a>

### getReviewTypesById

> Get the review type by id

Usage:

```js
const submissionAPI = require("tc-submission-api-wrapper");
const submissionAPIClient = submissionAPI(config);

const reviewTypeId = "a12a4180-65aa-42ec-a945-5fd21def1567";
submissionAPIClient
  .getReviewTypesById(reviewTypeId)
  .then(response => {
    // Use the response here
  })
  .catch(error => {
    // Handle the error here
  });
```

Params:

| Name         | Type   | Description                                   |
| ------------ | ------ | --------------------------------------------- |
| reviewTypeId | string | id of the reviewType that we want to retrieve |

<a id="method-explaination-headReviewTypesById"></a>

### headReviewTypesById

> Get only response status and headers information but no response body for the endpoint.

Usage:

```js
const submissionAPI = require("tc-submission-api-wrapper");
const submissionAPIClient = submissionAPI(config);

const reviewTypeId = "a12a4180-65aa-42ec-a945-5fd21def1567";
submissionAPIClient
  .headReviewTypesById(reviewTypeId)
  .then(response => {
    // Use the response here
  })
  .catch(error => {
    // Handle the error here
  });
```

Params:

| Name         | Type   | Description                                   |
| ------------ | ------ | --------------------------------------------- |
| reviewTypeId | string | id of the reviewType that we want to retrieve |

<a id="method-explaination-putReviewTypesById"></a>

### putReviewTypesById

> Update the review type.

Arguments: reviewTypeId, reqBody

Usage:

```js
const submissionAPI = require("tc-submission-api-wrapper");
const submissionAPIClient = submissionAPI(config);

const reqBody = {
  name: "Iterative Review",
  isActive: true
};
const reviewTypeId = "a12a4180-65aa-42ec-a945-5fd21def1567";
submissionAPIClient
  .putReviewTypesById(reviewTypeId, reqBody)
  .then(response => {
    // Use the response here
  })
  .catch(error => {
    // Handle the error here
  });
```

Params:

| Name         | Type   | Description                                                                                     |
| ------------ | ------ | ----------------------------------------------------------------------------------------------- |
| reviewTypeId | string | id of the reviewType that we want to update                                                     |
| reqBody      | Object | Required. The requestType that is to be updated. Refer the above example for the object format. |

<a id="method-explaination-patchReviewTypesById"></a>

### patchReviewTypesById

> Partially update the review type.

Arguments: reviewTypeId, reqBody

Usage:

```js
const submissionAPI = require("tc-submission-api-wrapper");
const submissionAPIClient = submissionAPI(config);

const reqBody = {
  isActive: true
};
const reviewTypeId = "a12a4180-65aa-42ec-a945-5fd21def1567";
submissionAPIClient
  .patchReviewTypesById(reviewTypeId, reqBody)
  .then(response => {
    // Use the response here
  })
  .catch(error => {
    // Handle the error here
  });
```

Params:

Params:

| Name         | Type   | Description                                                                                     |
| ------------ | ------ | ----------------------------------------------------------------------------------------------- |
| reviewTypeId | string | id of the reviewType that we want to update                                                     |
| reqBody      | Object | Required. The requestType that is to be updated. Refer the above example for the object format. |

<a id="method-explaination-deleteReviewTypesById"></a>

### deleteReviewTypesById

> Delete the review type.

Arguments: reviewTypeId

Usage:

```js
const submissionAPI = require("tc-submission-api-wrapper");
const submissionAPIClient = submissionAPI(config);

const reviewTypeId = "a12a4180-65aa-42ec-a945-5fd21def1567";
submissionAPIClient
  .deleteReviewTypesById(reviewTypeId)
  .then(response => {
    // Deleted successfully
  })
  .catch(error => {
    // Handle the error here
  });
```

Params:

| Name         | Type   | Description                                 |
| ------------ | ------ | ------------------------------------------- |
| reviewTypeId | string | id of the reviewType that we want to delete |
